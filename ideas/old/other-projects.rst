.. _gsoc-idea-other-projects:

Other projects
###############

.. card:: 

    :fas:`wave-square;pst-color-secondary` **Port FPP to BeagleBone AI/AM5729**
    ^^^^

    Falcon Player (FPP) is a distribution for Raspberry Pi's and BeagleBone Blacks that is used to 
    run synchronized light show, primarily for holiday lighting like Christmas and Halloween shows. 
    It's also used as a "firmware" for various capes used to connect wx281x Pixels and LED Panels to 
    Raspberry Pi and BeagleBone's. On the BeagleBone's, it uses the PRU's to handle the tight ws281x 
    timing which allows it to drive up to 48 strings of pixels. This project would involve porting 
    FPP to run on a BeagleBone AI. There would be several steps to this project including getting 
    basic FPP to install/run on the AI, getting the code to recognize the AI and differentiate that 
    to the users, updating the GPIO code to allow the user to configure GPIO for events, updating 
    the I2C handling to be able to recognize devices on the i2c bus, creating a new device tree to 
    reserve memory for data transfers, and porting the PRU code to the run on the AM5729 PRU's.

    - **Goal:** Get FPP fully up and running on a BeagleBone AI
    - **Hardware Skills:** Minimal
    - **Software Skills:** C++, PHP, PRU assembly
    - **Possible Mentors:** Daniel Kulp
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** 
        - https://github.com/FalconChristmas/fpp

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 

.. card:: 

    :fas:`wave-square;pst-color-secondary` **Support K3 Platform on buildroot**
    ^^^^

    - **Goal:** Support a minimal bootable system(headless containerized) for K3 platforms in upstream buildroot.
    - **Complexity:** 350 hours
    - **Hardware Skills:** Minimal
    - **Software Skills:** C, Linux Kernel, make/build
    - **Possible Mentors:** NishanthMenon
    - **Rating:** Medium
    - **Upstream Repository:** https://git.busybox.net/buildroot/
    - **References:** 
        - `buildroot homepage <https://buildroot.org/>`_

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`

.. card:: 

    :fas:`wave-square;pst-color-secondary` **Suspend to Disk on AI64/AM62**
    ^^^^

    - **Goal:** Enable suspend to disk functionality on the BBAI-64 or AM62
    - **Complexity:** 350 hours
    - **Hardware Skills:** Minimal
    - **Software Skills:** C, Linux Kernel, make/build
    - **Possible Mentors:** NishanthMenon, Dhruva Gole
    - **Rating:** Medium
    - **Upstream Repository:** TBD
    - **References:** 
        - `Arch wiki on Power Management <https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate>`_

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`

.. card:: 

    :fas:`wave-square;pst-color-secondary` **Enhance OpenOCD support for VSCode**
    ^^^^

    - **Goal:** Enhance OpenOCD support for VSCode / Eclipe Theia to include register, peripheral views
    - **Complexity:** 350 hours
    - **Hardware Skills:** Minimal
    - **Software Skills:** C, Linux Kernel, node.js, make/build
    - **Possible Mentors:** NishanthMenon
    - **Rating:** Medium
    - **Upstream Repository:** https://github.com/Marus/cortex-debug/ 
    - **References:** 
        - https://github.com/Marus/cortex-debug/blob/master/debug_attributes.md
        - https://github.com/Marus/cortex-debug/wiki 
        - https://github.com/dhoove/tixml2svd
    
    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`

.. card:: 

    :fas:`wave-square;pst-color-secondary` **Support BeagleBoard Flashing with balenaEtcher in Windows**
    ^^^^

    Today `balenaEtcher <https://www.balena.io/etcher/>`_ can be used to flash USB drives and SD cards, as well as some 
    eMMC’s for some SBC’s directly. This is possible today for BeagleBone devices in Linux and MacOS, but needs someone 
    with Windows Driver Framework experience to get it over the line for Windows users. We would expect the process to be 
    something like the following:

    1. Adjust the u-boot configuration and fix this issue: https://github.com/ravikp7/node-beagle-boot/issues/14
    2. Associate the right winusb drivers with the device using this module: https://github.com/balena-io-modules/winusb-driver-generator
    3. For reference, the source code for the plugin can be found here: https://github.com/balena-io-modules/node-beaglebone-usbboot and the u-boot image can be found in the `blobs` directory

    - **Goal:** Allow Windows users running balenaEtcher to flash BeagleBoard hardware
    - **Hardware Skills:** Minimal
    - **Software Skills:** Windows Driver Framework, U-boot, Typescript, C, C++
    - **Possible Mentors:** Zane Hitchcox and Joseph Kogut
    - **Expected Size of Project:** 350 hrs (less if very experienced with Windows Drivers)
    - **Rating:** Medium
    - **Upstream Repository:** https://github.com/balena-io/etcher
    - **References:** 
        - https://github.com/balena-io/etcher/issues/3477
    
    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`

.. card:: 

    :fas:`wave-square;pst-color-secondary` **BeagleBone AI64 U-Boot Upstream**
    ^^^^

    Currently, the BeagleBone AI64 is using a port of TI's u-boot branch, moving forward this should be posted for mainline u-boot.

    - **Complexity:** 40 hours
    - **Goal:** Booting Mainline U-Boot on the BeagleBone AI64
    - **Hardware Skills:** flashing MCU boards
    - **Software Skills:** C
    - **Possible Mentors:** jkridner, rcn-ee, NishanthMenon, Dhruva Gole
    - **Rating:** Medium
    - **Upstream Repository:** https://source.denx.de/u-boot/u-boot
    - **References:** 
        - `BeagleBoard U-boot branch <https://openbeagle.org/beagleboard/u-boot/-/tree/v2021.01-ti-08.05.00.001>`_
    
    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-info-line:`Small size`
    
.. card:: 

    :fas:`wave-square;pst-color-secondary` **BeagleBoard.org Drop usage of RNDIS for NCM**
    ^^^^

    Currently, BeagleBoard.org devices use a combination of RNDIS and NCM for network access over the usb connector.. Mainline kernel.org has decided to start removing support for RNDIS due to their stated reasons. Today NCM works out of the box for Linux and MacOS, we need to figure out how to write a driver for Windows or discover a way to use built-in drivers, just like we had done for RNDIS..

    - **Complexity:** 40 hours
    - **Goal:** Driver Less install on Windows for NCM drivers
    - **Hardware Skills:** flashing MCU boards
    - **Software Skills:** C
    - **Possible Mentors:** jkridner, rcn-ee
    - **Rating:** Medium
    - **Upstream Repository:**
    - **References:** 
        - `RNDIS hacks <https://openbeagle.org/beagleboard/repos-arm64/-/blob/main/bb-usb-gadgets/suite/bullseye/debian/bb-start-acm-ncm-rndis-old-gadget#L99-125>`_

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`